class CadastroProdutosPage < SitePrism::Page
    include Capybara::DSL

    set_url "/dashboard/cadastros/produtos"

    element :botao_Novo_Produto,:xpath, "//button[text()='Novo Produto']"

    #Produto
    element :campo_codigo, "input[name='codAlfa']"
    element :campo_nome, "input[name='descricao']"
    elements :campo_categoria, "div[class='css-1pcexqc-container react-select-white']"
    element :campo_valor_venda, "input[name='valorVenda']"
    element :campo_valor_custo, "input[name='valorCusto']"
    element :campo_estoque_min, "input[name='estoqueMinimo']"
    element :estoque_produto, :xpath, "//button[text()='Editar Estoque']"
    element :somar_estoque, :xpath, "//div[text()='Somar ao estoque']"
    element :quantidade_estoque, "input[name='quantidade']"
    element :nao_controlar_estoque, "input[name='noStock']"
    element :modificadores, :xpath, "//div[text()='Digite para pesquisar todos os modificadores']"
    element :origem_produto, :xpath, "//div[text()='Origem do produto']"
    element :ncm, :xpath, "//div[text()='Pesquise pelo código ou descrição']"
    element :grupo_regras, :xpath, "//div[text()='Digite para pesquisar as regras tributárias']"
    element :campo_observacao, "textarea[name='observacao']"
    elements :btn_salvar, "button[type='submit']"
    element :container_success, "h2[id='swal2-title']"
    element :btn_ok, :xpath, "//button[text()='OK']"
    

    element :btn_excluir_produto, :xpath, "//button[text()='Excluir']"
    element :btn_deletar_banner, :xpath, "//button[text()='Deletar']"
    element :msg_delete_sucess, "h2[id='swal2-title']"

    def clicarBotaoNovoProduto
        botao_Novo_Produto.click
    end

    def produtoComEstoque
        campo_codigo.set "1"
        campo_nome.set "Produto com estoque"
        campo_valor_venda.set "10"
        campo_valor_custo.set "5"
    end

end