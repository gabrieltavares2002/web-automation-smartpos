class CadastroCategoriasPage < SitePrism::Page
    include Capybara::DSL

set_url "/dashboard/cadastros/categorias"

element :botao_Nova_Categoria,:xpath, "//button[text()='Nova Categoria']"
element :campo_categoria_desc, "input[name='descricao']"
element :botao_salvar, :xpath, "//button[text()='Salvar']"
element :produto, :xpath, "//div[contains(@class, 'card-img-overlay')]"
element :botao_lapis,:xpath, " //*[contains(@cursor,  'pointer')]"
element :botao_excluir, :xpath,"//button[text()='Excluir']"
element :container_error, "h2[id='swal2-title']"


    def clicarBotaoNovaCategoria
    botao_Nova_Categoria.click
    end

    def criarcategoriaPComum
        campo_categoria_desc.set "Produto Comum" 
    end

    def criarcategoriaPVari
        campo_categoria_desc.set "Produto com Variação" 
    end

    def criarcategoriaPModi
        campo_categoria_desc.set "Produto com Modificador" 
    end

    def salvarCategoria
        sleep 2
        botao_salvar.click
    end

    def erroCategoriaDuplicada
    wait_until_container_error_visible
    end

end