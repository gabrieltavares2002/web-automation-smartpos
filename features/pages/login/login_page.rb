class LoginPage < SitePrism::Page 
    include Capybara::DSL

    set_url "/"

    element :campo_email, "input[type='email']"
    element :campo_senha, "input[type='password']"
    element :botao_entrar,:xpath, "//button[text()='ENTRAR']"
    element :user_logged, "img[alt='Catálogo Teste']"
    element :botao_sair, :xpath, "//button[text()='Sair']"

    def validarTelaInicio
        visit "/"
    end

    def realizarLogin(email, senha)
        campo_email.set email
        campo_senha.set senha
    end

    def clicarEntrar
        botao_entrar.click
    end

    def logOut
        user_logged.click
        botao_sair.click
    end

    def loginBasico(email, senha)
        visit "https://app-dev.smartpos.net.br"
        campo_email.set email
        campo_senha.set senha
        botao_entrar.click
    end

end
