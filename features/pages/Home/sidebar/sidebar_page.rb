class SideBarPage < SitePrism::Page
    include Capybara::DSL

    set_url "/dashboard"

    element :titulo_dash,:xpath,"//h1[text()='Dashboard']"

    #CADASTROS
    element :opc_cadastros, "i[id='layoutDashboard__register']"
    element :sub_opc_categorias,:xpath, "//span[text()='Categorias']"
    element :sub_opc_produtos, :xpath, "//span[text()='Produtos']"
    element :sub_opc_cliente, :xpath, "//span[text()='Clientes']"
    element :sub_opc_vendedores, :xpath, "//span[text()='Vendedores']"
    element :sub_opc_modificadores, :xpath, "//span[text()='Modificadores']"
    element :sub_opc_estoque, :xpath, "//span[text()='Edição de Estoque']"

    #RELATORIOS
    element :opc_relatorios,"i[id='layoutDashboard__reports']"

    #vendas
    element :sub_opc_consulta_de_vendas, :xpath, "//span[text()='Consulta de Vendas']"
    element :sub_opc_produtos_mais_vendidos, :xpath, "//span[text()='Produtos mais Vendidos']"
    element :sub_opc_vendas_por_tipo_pagamento, :xpath, "//span[text()='Vendas por Tipo de Pagamento']"
    element :sub_opc_vendas_por_vendedor, :xpath, "//span[text()='Vendas por Vendedor']"
    element :sub_opc_vendas_por_categoria, :xpath, "//span[text()='Vendas por Categorias']"

    #Caixa
    element :sub_opc_consulta_de_caixa, :xpath, "//span[text()='Consulta de Caixa']"
    element :sub_opc_entradas_e_saidas, :xpath, "//span[text()='Entradas e Saídas']"

    #Financeiro
    element :sub_opc_contas_a_pagar, :xpath, "//span[text()='Contas a pagar']"
    element :sub_opc_contas_a_receber, :xpath, "//span[text()='Contas a receber']"
    element :sub_opc_contas_de_cliente, :xpath, "//span[text()='Contas de Cliente']"
    element :sub_opc_vendas_por_cliente, :xpath, "//span[text()='Vendas por Clientes']"
    element :sub_opc_fluxo_financeiro, :xpath, "//span[text()='Fluxo Financeiro']"

    #Operacional
    element :sub_opc_rel_produtos, :xpath, "//span[text()='Produtos']"
    element :sub_opc_rel_aniversariantes, :xpath, "//span[text()='Aniversariantes']"


    #AREA FISCAL
    element :opc_area_fiscal, :xpath, "//span[text()='Área Fiscal']"

    element :sub_opc_grupos_regras_tributarias,:xpath, "//span[text()='Grupos de Regras Tributárias']"
    element :sub_opc_ativar_emissao_nota, :xpath, "//span[text()='Ativar emissão NFE/NFC-e']"
    element :sub_opc_notas_emitidas, :xpath, "//span[text()='Notas Emitidas']"
    element :sub_opc_historico_de_notas, :xpath, "//span[text()='Histórico de Notas do Contador']"
    element :sub_opc_inutilização_de_notas, :xpath, "//span[text()='Inutilização de Notas Fiscais']"

    #Configurações
    elements :opc_configuracoes, :xpath, "//span[text()='Configurações']"
    element :sub_opc_dados_de_negocio, :xpath, "//span[text()='Dados do Negócio']"

    #Loja Online
    element :opc_loja_online, :xpath, "//span[text()='Loja online']"
    element :pedidos_loja, :xpath, "//span[text()='Pedidos']"

    def tituloDash
        wait_until_titulo_dash_visible
    end

    #Cadastros
    def acessarCadastroCategoria
        opc_cadastros.click
        sub_opc_categorias.click
    end

    def acessarCadastroProduto
        opc_cadastros.click
        sub_opc_produtos.click
    end

    def acessarCadastroCliente
        opc_cadastros.click
        sub_opc_cliente.click
    end

    def acessarCadstroVendedor
        opc_cadastros.click
        sub_opc_vendedores.click
    end

    def acessarCadastroModificador
        opc_cadastros.click
        sub_opc_modificadores.click
    end

    def acessarEdicaoEstoque
        opc_cadastros.click
        sub_opc_estoque.click
    end


    #relatorios de vendas
    def acessarRelatorios
        opc_relatorios.click
    end

    def acessarConsultaVendas
        sub_opc_consulta_de_vendas.click
    end

    def acessarProdutosMaisVendidos
        sub_opc_produtos_mais_vendidos.click
    end

    def  acessarVendasPorTipoDepagamento
        sub_opc_vendas_por_tipo_pagamento.click
    end

    def acessarVendasPorVendedor
        sub_opc_vendas_por_vendedor.click
    end
    
    def acessarVendasPorCategoria
        sub_opc_vendas_por_categoria.click
    end


    #relatorios de Caixa
    def acessarConsultaDeCaixa
        sub_opc_consulta_de_caixa.click
    end
    
    def acessarEntradasESaidas
        sub_opc_entradas_e_saidas.click
    end

    #relatorios de financeiro
    def acessarContaApagar
        sub_opc_contas_a_pagar.click
    end

    def acessarContasAReceber
        sub_opc_contas_a_receber.click
    end

    def acessarContasAReceber
        sub_opc_contas_a_receber.click
    end

    def acessarContasDoCliente
        sub_opc_contas_de_cliente.click
    end

    def acessarVendasPorCliente
        sub_opc_vendas_por_cliente.click
    end

    def acessarFluxoFinanceiro
        sub_opc_fluxo_financeiro.click
    end

    #realtorios Operacional
    def acessarRelatorioProdutos
        sub_opc_rel_produtos.click
    end

    def acessarRelatorioAniversariantes
        sub_opc_rel_aniversariantes.click
    end


    #Area Fiscal
    def acessarAreaFiscal
        opc_area_fiscal.click
    end

    def acessarGruposRegrasTributarias
        sub_opc_grupos_regras_tributarias.click
    end

    def acessarAtivarEmissaoNF
        sub_opc_ativar_emissao_nota.click
    end

    def acessarNotasEmitidas
        sub_opc_notas_emitidas.click
    end

    def acessarHistoricoDenotas
        sub_opc_historico_de_notas.click
    end

    def acessarInutilizacaoNotas
        sub_opc_inutilização_de_notas.click
    end

end