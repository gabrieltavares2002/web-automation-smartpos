Dir[File.join(File.dirname(__FILE__),'../pages/*_page.rb')].each { |file| require file }

# Modulo para chamar as classes instanciadas
module Pages
    
    def login
        @login ||= LoginPage.new
    end

    def sidebar
        @sidebar ||= SideBarPage.new
    end
    
    def cadastroProdutos
        @cadastroProdutos ||= CadastroProdutosPage.new
    end

    def cadastroCategorias
        @cadastroCategorias ||= CadastroCategoriasPage.new
    end

    def cadastroModificador
        @cadastroModificador ||= CadastroModificadorPage.new
    end

    def cadastroCliente
        @cadastroCliente ||= CadastroClientePage.new
    end
    
    def cadastroUsuario
        @cadastroUsuario ||= CadastroUsuarioPage.new
    end
end
