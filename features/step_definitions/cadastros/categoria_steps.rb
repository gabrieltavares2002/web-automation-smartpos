Quando('acesso o Cadastro de categoria') do
    sidebar.acessarCadastroCategoria
  end
  
  Quando('clico no botão Nova Categoria') do
    cadastroCategorias.clicarBotaoNovaCategoria
  end
  
  Quando('digito o nome da nova categoria de produtos comuns') do
    cadastroCategorias.criarcategoriaPComum
  end
  
  Quando('clico no botão Salvar') do
    cadastroCategorias.salvarCategoria
  end
  
  Então('nova categoria deve ser exibida') do
    expect(page).to have_content "Categoria cadastrada com sucesso!"
    sleep 2
  end
  
  Quando('digito o nome da nova categoria de produtos com variação') do
    cadastroCategorias.criarcategoriaPVari
  end
  
  Quando('digito o nome da nova categoria de produtos com modificador') do
    cadastroCategorias.criarcategoriaPModi
  end