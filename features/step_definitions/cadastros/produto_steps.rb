Dado('que estou na tela de Dashboard') do
    sidebar.titulo_dash
  end
  
  Quando('acesso o Cadastro de produtos') do
    sidebar.acessarCadastroProduto
  end
  
  Quando('clico no botão Novo Produto') do
    cadastroProdutos.clicarBotaoNovoProduto
  end
  
  Quando('digito as caracteristicas do produto com produto com estoque') do
    cadastroProdutos.produtoComEstoque
  end
  
  Quando('coloco {int} em estoque') do |int|
  # Quando('coloco {float} em estoque') do |float|
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando('clico em Salvar') do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando('digito as caracteristicas do produto com produto com estoque zerado') do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando('digito as caracteristicas do produto com produto com estoque negativo') do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando('digito as caracteristicas do produto com produto sem controle de estoque') do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando('coloco a opção de não controlar estoque') do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando('digito as caracteristicas do produto com valor de venda zerado') do
    pending # Write code here that turns the phrase above into concrete actions
  end