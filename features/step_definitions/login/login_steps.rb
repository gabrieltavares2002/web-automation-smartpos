Dado('que estou na tela de login do site') do                                 
    login.validarTelaInicio
  end                                                                           
                                                                                
  Quando('informo {string} e {string} corretamente') do |email, senha|       
    login.realizarLogin(email, senha)
  end                                                                           
                                                                                
  Quando('clico no botão Entrar') do                                            
    login.clicarEntrar
  end                                                                           
                                                                                
  Então('é apresentada a tela de DashBoard.') do                                
    sleep 5
    expect(page).to have_content "Dashboard"
  end                                                                           
                                                                                
  Então('clico em logout') do                                                   
    login.logOut
  end                                                                           
                                                                                
  Então('o App retorna para a tela de login.') do                               
    login.validarTelaInicio 
  end                                                                           
                                                                                
  Quando('informo {string} e {string}') do |email, senha|               
    login.realizarLogin(email, senha) 
  end                                                                           
                                                                                
  Então('é apresentada mensagem {string}.') do |mensagem|                         
    expect(page).to have_content(mensagem)
  end                                                                           