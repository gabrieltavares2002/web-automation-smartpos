#language:pt
# encoding: utf-8

@login
@cadastroCategorias

Funcionalidade: Cadastrar categoria no dashboard

    Contexto:
    Dado que estou na tela de Dashboard
    Quando acesso o Cadastro de categoria

    @categoriaProdutoComum
    Cenário: Cadastrar categoria de produtos comuns
    E clico no botão Nova Categoria
    E digito o nome da nova categoria de produtos comuns
    E clico no botão Salvar
    Então nova categoria deve ser exibida

    @categoriaProdutoComModificador
    Cenário: Cadastrar categoria de produtos com modificador
    E clico no botão Nova Categoria
    E digito o nome da nova categoria de produtos com modificador
    E clico no botão Salvar
    Então nova categoria deve ser exibida

    @categoriaProdutoComVariacao
    Cenário: Cadastrar categoria de produtos com variação
    E clico no botão Nova Categoria
    E digito o nome da nova categoria de produtos com variação
    E clico no botão Salvar
    Então nova categoria deve ser exibida

   