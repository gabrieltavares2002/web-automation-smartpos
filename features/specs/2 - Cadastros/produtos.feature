#language:pt
# encoding: utf-8

@login
@testeproduto

Funcionalidade: Cadastrar produto no dashboard

    Contexto:
    Dado que estou na tela de Dashboard
    Quando acesso o Cadastro de produtos

    @produtoComEstoque
    Cenário: Cadastro de produto com estoque
    E clico no botão Novo Produto
    E digito as caracteristicas do produto com produto com estoque
    E coloco 1000 em estoque
    E clico em Salvar

    @produtoComEstoqueZerado
    Cenário: Cadastro de produto com estoque zerado
    E clico no botão Novo Produto
    E digito as caracteristicas do produto com produto com estoque zerado
    E coloco 0 em estoque
    E clico em Salvar

    @produtoComEstoqueNegativo
    Cenário: Cadastro de produto com estoque negativo
    E clico no botão Novo Produto
    E digito as caracteristicas do produto com produto com estoque negativo
    E coloco -1 em estoque
    E clico em Salvar

    @ProdutoSemControleDeEstoque
    Cenário: Cadastro de produto sem controle de estoque
    E clico no botão Novo Produto
    E digito as caracteristicas do produto com produto sem controle de estoque
    E coloco a opção de não controlar estoque
    E clico em Salvar

    @ProdutoComValorDeVendaZerado
    Cenário: Cadastro de produto com valor de venda zerado
    E clico no botão Novo Produto
    E digito as caracteristicas do produto com valor de venda zerado
    E clico em Salvar