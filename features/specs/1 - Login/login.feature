#language: pt
# encoding: utf-8
@testelogin

Funcionalidade: Realizar Login

Contexto:
    Dado que estou na tela de login do site

  @loginsucesso
  Cenario: Login com sucesso 
    Quando informo "catalogo@netpos.com" e "123456" corretamente
    E clico no botão Entrar
    Então é apresentada a tela de DashBoard.

  @login_logout
  Cenario: Logout
    Quando informo "catalogo@netpos.com" e "123456" corretamente
    E clico no botão Entrar
    Então é apresentada a tela de DashBoard.
    E clico em logout
    E o App retorna para a tela de login.

  @loginSemSucesso
  Esquema do Cenario: Login sem sucesso
    Quando informo <email> e <senha> 
    E clico no botão Entrar
    Então é apresentada mensagem <msg>.  

    Exemplos:
    |email                          |senha      |msg                                            | 
    |"catalogo@netpos.com"          |""         |"Digite sua senha"                             |
    |""                             |"ps654321" |"Digite um e-mail válido"                      |
    |""                             |""         |"Digite um e-mail válido"                      |
    |""                             |""         |"Digite sua senha"                             |
    |"catalogo@netpos.com"          |"ps123456" |"Usuário ou senha inválidos"                   |
    |"catalogo@naoexiste.com"       |"123456"   |"Usuário ou senha inválidos"                   |
    |"catalogo@netpos.com"          |"12345"    |"Digite uma senha com mais de cinco carecteres"|

  



  



